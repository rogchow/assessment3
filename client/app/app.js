(function () {
    angular
        .module("resourceApp", [
            "ngFileUpload",
            "ui.router",
            //"ngFlash",
            //"ngSanitize",
            //"ngProgress",
            "ngMessages"
            //"data-table"
        ]);
})();