(function () {
    angular
        .module("resourceApp")
        .config(resourceAppConfig);
    resourceAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function resourceAppConfig($stateProvider,$urlRouterProvider) {
        $urlRouterProvider.otherwise("/login");

        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: "/login.html"
            })

            .state('userpage',{
                url:'/booking',
                templateUrl: "/app/consumer/userpage.html"
            })

            .state('user_welcome',{
                url:'/welcome',
                template: "/app/consumer/user_welcome.html",
                // parent:'userpage'
            })

            .state('schedule',{
                url:'/schedule',
                templateUrl: 'app/consumer/schedule.html',
                // parent:'userpage'
            })

            .state('bookingschedule',{
                url:'/schedule',
                templateUrl: "app/consumer/bookingschedule.html"
                //controller:'bookingschedule',
                //controllerAs: 'Ctrl'
            })

            .state('currentbooking',{
                url:'/currentbooking',
                templateUrl:"app/consumer/currentbooking.html"
            })

            .state('newbooking',{
                url:'/newbooking/:service_id/:start_time',
                templateUrl:"app/consumer/newbooking.html",
                // parent:'userpage'
            })

            .state('confirmation',{
                url:'/bookingconfirmation',
                templateUrl:"app/consumer/bookingconfirmation.html"
            })

            .state('scheduleadmin', {
                url : '/admin',
                templateUrl: "app/admin/scheduleadmin/scheduleadmin.html" 
            })

           
            .state("scheduleadmin.createsp",{
                views: {
                    "createsp@scheduleadmin": {
                        templateUrl: "app/admin/scheduleadmin/createsp.html"
                    }
                }    
            })   

            .state("scheduleadmin.createservicename", {
                views: {
                    "createsp@scheduleadmin": {
                        templateUrl: "app/admin/scheduleadmin/createsp.html"
                        },
                     "createservicename@scheduleadmin": {
                        templateUrl: "app/admin/scheduleadmin/createservicename.html"
                    }
                }    
            })

             .state("scheduleadmin.createschedule", {
                    views: {
                        "createsp@scheduleadmin": {
                        templateUrl: "app/admin/scheduleadmin/createsp.html"
                        },
                         "createservicename@scheduleadmin": {
                        templateUrl: "app/admin/scheduleadmin/createservicename.html"
                         },
                        "createschedule@scheduleadmin": {
                            templateUrl: "app/admin/scheduleadmin/createschedule.html",
                        } 
                    }
            })

            .state("scheduleexception",{
                views: {
                        "createsp@scheduleadmin": {
                        templateUrl: "app/admin/scheduleadmin/createsp.html"
                        },
                         "createservicename@scheduleadmin": {
                        templateUrl: "app/admin/scheduleadmin/createservicename.html"
                         },
                        "createschedule@scheduleadmin": {
                            templateUrl: "app/admin/scheduleadmin/createschedule.html",
                        },
                        "scheduleexception@scheduleadmin":{
                            templateUrl: "app/admin/scheduleadmin/scheduleexception.html",
                        }
                    }
            })

            .state("scheduleadmin.schedulepreview",{
                 views: {
                        "createsp@scheduleadmin": {
                        templateUrl: "app/admin/scheduleadmin/createsp.html"
                        },
                         "createservicename@scheduleadmin": {
                        templateUrl: "app/admin/scheduleadmin/createservicename.html"
                         },
                        "createschedule@scheduleadmin": {
                            templateUrl: "app/admin/scheduleadmin/createschedule.html",
                        },
                        // "scheduleexception@scheduleadmin":{
                        //     templateUrl: "app/admin/scheduleadmin/scheduleexception.html",
                        // },
                        "schedulepreview@scheduleadmin":{
                            templateUrl: "app/admin/scheduleadmin/schedulepreview.html",
                        }
                    }
            });
    }
})();