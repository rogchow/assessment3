(function(){
    angular
        .module("resourceApp")
        .controller("adminCtrl", adminCtrl);

    adminCtrl.$inject = [ '$state', 'AuthFactory'];

    function adminCtrl($state, AuthFactory){
        var vm = this;

        vm.logout = logout;

        function logout() {
            //console.log("logout()");

            AuthFactory.logout()
            .then(function(result) {
                $state.go('login');
            })
            .catch(function(err) {
                $state.go('login');
            });
        }

        AuthFactory.getUserStatus(function(data) {
            if (!data) {
                $state.go("login");
            }
        });
    }
})();
