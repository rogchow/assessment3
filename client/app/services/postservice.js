(function(){
    angular
        .module("resourceApp")
        .service("postService", postService);

    postService.$inject = ['$http'];

    function postService($http){
        var service = this;
        service.insertNewBooking = insertNewBooking;

        function insertNewBooking(service_id){
            return $http({
                method: 'put',
                url: "/insertNewBooking/" + service_id
            });
        }
    }
})();