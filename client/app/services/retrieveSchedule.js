(function(){
    angular
        .module("resourceApp")
        .service("retrieveScheduleSvs", retrieveScheduleSvs);

    retrieveScheduleSvs.$inject = ['$http'];

    function retrieveScheduleSvs($http){
        var service = this
        service.retrieveSchedule = retrieveSchedule;
        service.retrievebooking = retrievebooking;
        // service.retrieveScheduleById = retrieveScheduleById;
        // service.retrieveScheduleByCustomer = retrieveScheduleByCustomer;
        // service.retrieveScheduleByDate = retrieveScheduleByDate;

        function retrieveSchedule(){
            console.log("in service retrieveSchedule");

            return $http({
                method: 'get',
                url: "/retrieveSchedule"
            });
            // .then(function(result) {
            //     return result;
            // })
            // .catch(function(err) {
            //     return err;
            // });
        }

        function retrievebooking(){
            console.log("retrieve schedule by Id");
            return $http({
                method: 'get',
                url: "/retrieveScheduleById"
                // data : {currentbooking:result}
            });


        }

        // function retrieveScheduleByCustomer(customer_id){
        //     return $http({
        //         method:'get',
        //         url: "/retrieveScheduleByCustomer",
        //         data: {Schedules: Schedules}
        //         });
        //     };

        // }

        // function retrieveScheduleByDate(date){
        //     return $http({
        //         method:'get',
        //         url: "/retrieveScheduleByCustomer",
        //         data: {Schedules: Schedules}
        //         });
        //     };

        // }
    }
})();