(function(){
    angular
        .module("resourceApp")
        .controller("scheduleCtrl", scheduleCtrl);

scheduleCtrl.$inject = ['$state','retrieveScheduleSvs', 'postService','AuthFactory'];

function scheduleCtrl($state, retrieveScheduleSvs, postService, AuthFactory){
    var vm = this;

    vm.date = "";
    vm.start_time = $state.params.start_time;
    vm.service_id = "";
    vm.schedule = "";
    vm.bookingmsg = "Click to confirm your booking.";
    vm.getSchedule = getschedule;
    vm.selectschedule = selectschedule;
    vm.addbooking = addbooking;
    vm.retrievebooking = retrievebooking;
    vm.logout = logout;
    vm.currentbooking = "";
    vm.initCall =  initCall;

    // getschedule()
    console.log("** inside schedule.js");

    function initCall() {
        console.log("initCall()");
        retrievebooking();
        console.log("initialising");
    }

    function getschedule(){
        console.log("getschedule()");
        retrieveScheduleSvs
            .retrieveSchedule()
            .then(function (results) {
                    vm.schedule = results.data;
                    console.log (JSON.stringify(results.data));
                })
                .catch(function (err) {
                    console.log("error " + err);
                });
         }

    function selectschedule(){
        console.log("select schedule");
        $state.go('newbooking', {service_id: service_id});
        }

    function addbooking(){
        console.log("addbooking in progress")
        postService
            .insertNewBooking($state.params.service_id)
            .then(function(result){
                vm.bookingmsg = "Booking confirmed! You have opted for SMS notification 24 hours before appointment";
            })
            .catch(function(err){
                //do something
                console.log("error " + err);
            });


        }

    function retrievebooking(){
        console.log("retrieving current booking")
        retrieveScheduleSvs
            .retrievebooking()
            .then(function(result){
                vm.currentbooking = result.data;
                //console.log(vm.currentbooking);
                //$state.go('currentbooking');
            })
            .catch(function(err){
                //do something
                console.log("error " + err);
            });
        }

    function logout() {
            AuthFactory.logout()
            .then(function(result) {
                $state.go('login');
            })
            .catch(function(err) {
                $state.go('login');
            });
        }

    }

})();