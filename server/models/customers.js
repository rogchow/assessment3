module.exports = function(sequelize, Sequelize) {
    var customers =  sequelize.define('customers', {
        customer_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },

            email: {
            type: Sequelize.STRING,
            allowNull: false,
        },

            password: {
            type: Sequelize.STRING,
            allowNull: false
       },

            name: {
            type: Sequelize.STRING,
            allowNull: false
              },
    
            phone_primary: {
            type: Sequelize.STRING,
            allowNull: false
       },

            address: {
            type: Sequelize.STRING,
            allowNull: false
       },
    },   
       {
        timestamps: true,
        freezeTableName: true,
        createdAt: true,
        updateAt:true
       });

       return customers
}