module.exports = function(sequelize, Sequelize) {
    var users =  sequelize.define('users', {
        idusers: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },

        usertype: {
            type: Sequelize.INTEGER,
            allowNull: false
        },

        email: {
            type: Sequelize.STRING,
            allowNull: false
        },

        password: {
            type: Sequelize.STRING,
            allowNull: false
        },
    
        source: {
            type: Sequelize.STRING,
            allowNull: true
        }
    }, {
        timestamps: true,
        tableName: 'users',
    });

    return users;
};
