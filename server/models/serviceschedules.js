module.exports = function(sequelize, Sequelize) {
    var serviceSchedule =  sequelize.define('service_schedules', {
        service_id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },

        service_name: {
            type: Sequelize.STRING,
            allowNull: false,
        },

        sp_id: {
            type: Sequelize.INTEGER,
            allowNull: false
       },

       start_time: {
           type: Sequelize.DATE,
            allowNull: false
              },
    
       end_time: {
            type: Sequelize.DATE,
            allowNull: false
       },

       status: {
            type: Sequelize.INTEGER,
            allowNull:true
       },

       contract_id: {
            type: Sequelize.INTEGER,
            allowNull:true
       },

       customer_id: {
            type: Sequelize.INTEGER,
            allowNull:true
       }
    },   
       {
        timestamps: true,
        freezeTableName: true
       });

       return serviceSchedule
}

