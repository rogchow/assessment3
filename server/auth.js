var LocalStrategy = require("passport-local").Strategy;
//var bcrypt   = require('bcryptjs');

// Loads sequelize ORM
//var Sequelize = require("sequelize");

//var customers = require('./models/customers')(sequelize, Sequelize);
//var User = require("./models/users").User;
//var AuthProvider = require("./models/users").AuthProvider;
//var config = require("./config");

//Setup local strategy
module.exports = function (users, app, passport) {
     passport.use(new LocalStrategy({
        usernameField: "email",
        passwordField: "password"
    }, function(email, password, done) {
        //console.log(email + " " + password);
        users.findOne({
            attributes: [ 'email', 'usertype' ],
            where: {
                email: email,
                password: password
            }
        }).then(function(result) {
            console.log(result.dataValues);
            if (!result){
                return done(null, false);
            } else {
                return done(null, result.dataValues);
            }
        }).catch(function(err){
            console.log(err);
            return done(err, false);
        });
    }));

    passport.serializeUser(function (user, done) {
        console.info("serial to session");
        console.log(user);
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        users.findOne({
            attributes: [ 'email', 'usertype' ],            
            where: {
                email: user.email
            }
        }).then(function(result) {
            if(result){
                done(null, result.dataValues);
            }
            else {
                done(null, false);
            }
        }).catch(function(err){
            done(err);
        });
    });

};
