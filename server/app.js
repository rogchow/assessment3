var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var cookieParser = require('cookie-parser');
var session = require("express-session");
var passport = require("passport");
var flash = require('connect-flash');

var app = express();

app.use(flash());
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Loads sequelize ORM
var Sequelize = require("sequelize");

// Defines MySQL configuration
const MYSQL_USERNAME = 'root';
const MYSQL_PASSWORD = 'MyNewPass';

var sequelize = new Sequelize(
        'resource_schedule',
        MYSQL_USERNAME,
        MYSQL_PASSWORD,
        {
            host: 'localhost',         // default port    : 3306
            logging: console.log,
            dialect: 'mysql',
            pool: {
                max: 5,
                min: 0,
                idle: 10000
            }
        }
    );

// Loads model for schedule, users and customers table
var serviceSchedule = require('./models/serviceschedules')(sequelize, Sequelize);
var users = require('./models/users')(sequelize, Sequelize);
var customers = require('./models/customers')(sequelize, Sequelize);


const NODE_PORT = process.env.PORT || 3000;

//Define routes
app.use(express.static(__dirname + '/../client/'));


// Initialize session
app.use(session({
    secret: "ressourceApp-secret",
    resave: false,
    saveUninitialized: true
}));

//Initialize passport
app.use(passport.initialize());
app.use(passport.session());

require('./auth')(users, app, passport);

//login endpoint
app.post("/login", passport.authenticate('local', {
    failureRedirect:'/index.html'
}),
function(req,res){
    console.log("In /login success");
    console.log(req.user);
    //res.redirect('app/admin/scheduleadmin/scheduleadmin.html')
    res.status(202).json(req.user);
});

// get user status
app.get("/status/user", function (req, res) {
    var status = "";
    if(req.user) {
        status = req.user.email;
    }
    console.info("status of the user --> " + status);
    res.send(status).json(req.user);
});

//logout endpoint
app.get("/logout", function(req, res) {
    console.log("loggin out");
    req.logout();             // clears the passport session
    req.session.destroy();    // destroys all session related data
    res.status(202).end();
});


//Protected route
app.put("/insertNewBooking/:service_id", isAuthenticated, function(req,res){
    console.log("inserting new booking...")
    var customer_id = "";
    var where = {service_id: req.params.service_id};
    console.log("Update Data: " + JSON.stringify(req.body));

    customers
        .findOne({
            attributes: ['email', 'customer_id'],
            where: {
                email: req.user.email
            }
        })
        .then(function(result) {
            console.log(result.dataValues);
            customer_id = result.dataValues.customer_id;

            serviceSchedule
                .update({
                    customer_id: customer_id
                },
                {
                    where: where 
                })
                .then(function(results) {  
                    res.status(200).end();  
                })
                .catch(function(err) {
                    res.status(500).end();
                });
        });
});

app.get("/retrieveScheduleById", isAuthenticated, function(req, res){
    var customer_id = "";
    var where = {email: req.user.email};
    console.log("Update Data: " + JSON.stringify(req.body));

         customers
            .findOne
                ({
                attributes: ['email', 'customer_id'],
                where: { 
                    email: req.user.email
                }
                }).then(function(result){
                console.log(result.dataValues);
                customer_id = result.dataValues.customer_id;
                    serviceSchedule
                        .findOne({
                            where: {
                                customer_id: customer_id
                            }
                        }).then(function(result){
                            console.log(result.dataValues.start_time)
                            res.status(200).json(result.dataValues);
                        }).catch(function(err){
                            res.status(500).end();
                        });
                })
});

app.get("/retrieveSchedule", isAuthenticated, function(req, res){
    console.log('/retrieveSchedule');
    var curdate = new Date();

        serviceSchedule
            .findOne({
                attributes: [
                    'service_id','start_time', 'service_name'
                ],
                where: {
                    start_time: {
                        $gt: curdate
                    }
                },
                order: 'start_time ASC'
            })
            .then(function(results) {
                console.log(results.dataValues);

                curdate = new Date(results.dataValues.start_time);
                var nextdate = new Date(results.dataValues.start_time);
                nextdate.setDate(curdate.getDate()+7);

                serviceSchedule
                    .findAll({
                        attributes: [
                            'service_id', 'start_time', 'service_name'
                        ],
                        where: { 
                            start_time: {
                                $gt: curdate,
                                $lt: nextdate
                            }
                        }
                    }).then(function(results){
                        //console.log(results);
                        res
                            .status(200)
                            .json(results);
                    }).catch(function(err){
                        console.log(err);
                        res
                            .status(500)
                            .json(err);
                    })

            })
            .catch(function(err) {
                console.log(err);
            });


});

app.get("/retrieveScheduleById", isAuthenticated, function(req, res){
    var customer_id = "";
    attributes: ['email', 'customer_id'],
         customers
            .findOne({
                where: { 
                    email: req.user.email
                }
            }).then(function(service_schedules){
                console.log(result.dataValues);
                customer_id = result.dataValues.customer_id;
                    serviceschedule
                        .fineOne({
                            where: {
                                customer_id: customer_id
                            }
                        })
                    .status(200)
                    .json(service_schedules);
            }).catch(function(err){
                res
                    .status(500)
                    .json(err);
            })

});

app.get("/retrieveScheduleByDate", isAuthenticated, function(req, res){
    // console.log(req.schedulestartdate);
    //      service_schedules
    //         .findAll({
    //             where: { 
    //                 Date = Date
    //             }
    //         }).then(function(service_schedules){
    //             res
    //                 .status(200)
    //                 .json(service_schedules);
    //         }).catch(function(err){
    //             res
    //                 .status(500)
    //                 .json(err);
    //         })
});


function isAuthenticated(req, res, next) {
    console.log(req.url);
    console.log(req.user);
    if (req.isAuthenticated()) return next();
    res.redirect("/index.html");
}
app.listen(NODE_PORT, function () {
    console.log("Server running at PORT: " + NODE_PORT);
});